# OpenML dataset: fourclass

https://www.openml.org/d/1435

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Tin Kam Ho and Eugene M. Kleinberg.  
**Source**: [original](http://www.csie.ntu.edu.tw/~cjlin/libsvmtools/datasets/binary.html) - Date unknown  
**Please cite**: Tin Kam Ho and Eugene M. Kleinberg.
Building projectable classifiers of arbitrary complexity.
In Proceedings of the 13th International Conference on Pattern Recognition, pages 880-885, Vienna, Austria, August 1996.  

#Dataset from the LIBSVM data repository.

Preprocessing: transform to two-class

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1435) of an [OpenML dataset](https://www.openml.org/d/1435). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1435/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1435/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1435/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

